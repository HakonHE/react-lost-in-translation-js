module.exports = {
  content: ["./src/**/*.{js,jsx}"],
  theme: {
    extend: {
      colors: {
        YELLOW: "#FFC75F",
        YELLOW_DARK: "#E7B355",
        PURPLE: "#845EC2",
        NIGHT_BLUE: "#141852",
        DARK_NIGHT_BLUE: "#070B34",
        LIGHT_NIGHT_BLUE: "#2B2F77",
        NIGHT_PURPLE: "#483475",
        NIGHT_PURPLE_LIGHT: "#6B4984",
        LIGHTER_NIGHT_PURPLE: "#855988",
        MOON_WHITE: "#F0EFE2",
        MOON_LIGHT_GRAY: "#D0D0CA",
        MOON_DARK_GRAY: "#A1A19C"
      }
    },
  },
  plugins: [],
}
