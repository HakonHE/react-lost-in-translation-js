# Lost in Translation App

An app to translate words and sentences to sign language. 

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Fonts and Colors 

Extra Colors: \
PURPLE: "#845EC2" \
NIGHT_BLUE: "#141852" \
DARK_NIGHT_BLUE: "#070B34" \
LIGHT_NIGHT_BLUE: "#2B2F77" \
NIGHT_PURPLE: "#483475" \
NIGHT_PURPLE_LIGHT: "#6B4984" \
LIGHTER_NIGHT_PURPLE: "#855988" \
MOON_WHITE: "#F0EFE2" \
MOON_LIGHT_GRAY: "#D0D0CA" \
MOON_DARK_GRAY: "#A1A19C" 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Author

Håkon Holm Erstad
