import "./App.css"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import Login from "./views/Login"
import Profile from "./views/Profile"
import Translation from "./views/Translation"
import Navbar from "./components/Navbar/Navbar"

function App() {
	return (
		<BrowserRouter>
			<div className="App m-8 mx-12 max-w-full">
				<Navbar />
				<Routes>
					<Route path="/" element={<Login />} />
					<Route path="/translation" element={<Translation />} />
					<Route path="/profile" element={<Profile />} />
				</Routes>
			</div>
		</BrowserRouter>
	)
}

export default App
