import { useSelector } from "react-redux"
import ProfileList from "../components/Profile/ProfileList"
import { useDispatch } from "react-redux"
import { deleteTranslation } from "../features/translations/translationSlice"
import { deleteUserId, deleteUsername } from "../features/user/userSlice"
import { deleteApiTranslations } from "../api/translation"
import withAuth from "../hoc/withAuth"

const Profile = () => {
	const translations = useSelector((state) => state.translation.translations)
	const username = useSelector((state) => state.user.username)
	const userId = useSelector((state) => state.user.userId)
	const translationList = translations
		.slice(-10)
		.map((translation, index) => <ProfileList key={index} translation={translation} />)

	const dispatch = useDispatch()

	const handleClearHistory = async () => {
		if (window.confirm("Are you sure you want to clear history?")) {
			dispatch(deleteTranslation())
			const [error] = await deleteApiTranslations(userId)
		}
	}

	const handleLogout = () => {
		if (window.confirm("Are you sure you want to logout?")) {
			dispatch(deleteUsername())
			dispatch(deleteUserId())
			dispatch(deleteTranslation())
			localStorage.clear()
		}
	}

	return (
		<>
			<div className="container shadow-lg rounded-b-md">
				<div className="h-12 bg-gradient-to-b from-NIGHT_BLUE to-LIGHTER_NIGHT_PURPLE border-t border-black text-white flex flex-row justify-between">
					<div>
						<p className="text-lg ml-4 mt-2">User: {username}</p>
					</div>
					<div>
						<button
							onClick={handleClearHistory}
							className="bg-transparent rounded-full px-3 py-1 mr-2 my-1 border-2 border-white hover:border-sky-500 hover:text-sky-500 tracking-wide"
						>
							Clear History
						</button>
						<button
							onClick={handleLogout}
							className="bg-transparent rounded-full px-3 py-1 mr-8 my-1 border-2 border-white hover:border-sky-500 hover:text-sky-500 tracking-wide"
						>
							Logout
						</button>
					</div>
				</div>
				<div className="h-96 flex flex-col mx-4 mt-2">
					<p className="mb-1 ml-4 text-xl font-semibold underline underline-offset-2 decoration-NIGHT_BLUE">
						Translation History:
					</p>
					<ul className="border-2 border-LIGHTER_NIGHT_PURPLE rounded-md h-72 mx-4 px-2 py-1 overflow-auto">
						{translationList}
					</ul>
				</div>
			</div>
		</>
	)
}

export default withAuth(Profile)
