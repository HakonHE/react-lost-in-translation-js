import { useState } from "react"
import { useDispatch } from "react-redux"
import { useSelector } from "react-redux"
import { updateApiTranslations } from "../api/translation"
import TranslationForm from "../components/Translation/TranslationForm"
import { updateTranslation } from "../features/translations/translationSlice"
import withAuth from "../hoc/withAuth"

const ALPHABET = [
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"k",
	"l",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"u",
	"v",
	"w",
	"x",
	"y",
	"z",
]

/**
 * Takes a string and checks if it matches any special characters.
 * @param {*} str
 * @returns Boolean
 */
function containsSpecialChars(str) {
	const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/
	return specialChars.test(str)
}

/**
 * Takes a string and checks if it matches a number.
 * @param {*} str
 * @returns
 */
function containsNumber(str) {
	const numbers = /\d+/
	return numbers.test(str)
}

const Translation = () => {
	const dispatch = useDispatch()
	const userId = useSelector((state) => state.user.userId)
	const translations = [useSelector((state) => state.translation.translations)]
	const characterSigns = []
	const [translatedOutput, setTranslatedOutput] = useState([])
	const [apiError, setApiError] = useState(null)

	const assigningSigns = () => {
		for (let letter in ALPHABET) {
			characterSigns.push({
				image: `images/signs/${ALPHABET[letter]}.png`,
			})
		}
	}

	const handleTextToBeTranslated = async (textToTranslate) => {
		assigningSigns()

		const [error, user] = await updateApiTranslations(userId, translations, textToTranslate)
		if (error !== null) {
			setApiError(error)
		}

		if (user !== null) {
			dispatch(updateTranslation(textToTranslate))
			const items = textToTranslate.split("").map((char) => {
				if (char === " " || containsSpecialChars(char) || containsNumber(char)) return ""
				else {
					return (
						<img
							src={"images/signs/" + char.toLowerCase() + ".png"}
							alt={"letter " + char.toLowerCase() + " in sign language"}
							width="55"
						/>
					)
				}
			})
			setTranslatedOutput(items)
		}
	}

	return (
		<>
			<div className="container shadow-lg">
				<section className="h-40 bg-gradient-to-b from-NIGHT_BLUE to-LIGHTER_NIGHT_PURPLE border-t border-black text-white">
					<TranslationForm onText={handleTextToBeTranslated} />
				</section>
				<section className="container relative h-60">
					<div className="relative shadow-lg h-52 max-w-full rounded-md mt-8 mb-5 mx-20 border-2 border-LIGHTER_NIGHT_PURPLE overflow-auto">
						<ul className="flex flex-row justify-start mx-4 mb-4 flex-wrap basis-1/12">
							{translatedOutput}
						</ul>
					</div>
					<div className="relative rounded-b-md mb-2 bg-LIGHTER_NIGHT_PURPLE flex flex-row justify-center">
						<p className="text-white">Translation</p>
					</div>
				</section>
				{apiError && <p>{apiError}</p>}
			</div>
		</>
	)
}

export default withAuth(Translation)
