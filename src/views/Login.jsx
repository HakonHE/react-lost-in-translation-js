import LoginForm from "../components/Login/LoginForm"

const Login = () => {
	return (
		<>
			<div className="container relative grid grid-cols-2 h-48 bg-gradient-to-b from-NIGHT_BLUE to-LIGHTER_NIGHT_PURPLE border-t border-black text-white">
				<img
					src={"images/Logo.png"}
					alt="logo"
					width="100"
					className="absolute top-5 left-36 bg-white rounded-full p-1"
				/>
				<h1 className="absolute top-6 left-64 text-3xl font-bold">Lost in Translation</h1>
				<h3 className="absolute top-16 left-72 text-xl">Get started</h3>
			</div>
			<div className="container relative shadow-lg rounded-b-md h-56">
				<LoginForm />
			</div>
		</>
	)
}

export default Login
