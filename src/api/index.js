const apiKey = process.env.REACT_APP_API_KEY

/**
 * Creates headers needed for sending requests to the API.
 * @returns {Object} of header options
 */
export const createHeaders = () => {
	return {
		"Content-Type": "application/json",
		"x-api-key": apiKey,
	}
}
