import { createHeaders } from "./index"

const apiUrl = process.env.REACT_APP_API_URL

/**
 * Updates the translation of the corresponding user in the API.
 * @param {Number} userId
 * @param {String} translation
 * @param {String} newTranslation
 * @returns {Object|Error}
 */
export const updateApiTranslations = async (userId, translation, newTranslation) => {
	try {
		const response = await fetch(`${apiUrl}/${userId}`, {
			method: "PATCH", // NB: Set method to PATCH
			headers: createHeaders(),
			body: JSON.stringify({
				translations: [...translation, newTranslation],
			}),
		})
		if (!response.ok) {
			throw new Error("Could not update translations history")
		}
		const user = await response.json()
		return [null, user]
	} catch (error) {
		return [error.message, null]
	}
}

/**
 * Clears the translation history of the corresponding user in the API.
 * @param {Number} userId
 * @returns {Object|Error}
 */
export const deleteApiTranslations = async (userId) => {
	try {
		const response = await fetch(`${apiUrl}/${userId}`, {
			method: "PATCH", // NB: Set method to PATCH
			headers: createHeaders(),
			body: JSON.stringify({
				translations: [],
			}),
		})
		if (!response.ok) {
			throw new Error("Could not delete translations history")
		}
		const user = await response.json()
		return [null, user]
	} catch (error) {
		return [error.message, null]
	}
}
