import { createHeaders } from "./index"

const apiUrl = process.env.REACT_APP_API_URL

/**
 * Checks if the user provided already exists.
 * @param {String} username
 * @returns {Object|Error}
 */
const checkForUser = async (username) => {
	try {
		const response = await fetch(`${apiUrl}?username=${username}`)
		if (!response.ok) {
			throw new Error("Could not complete request.")
		}
		const data = await response.json()
		return [null, data]
	} catch (error) {
		return [error.message, []]
	}
}

/**
 * Creates a user in the API database.
 * @param {String} username
 * @returns {Object|Error}
 */
const createUser = async (username) => {
	try {
		const response = await fetch(apiUrl, {
			method: "POST",
			headers: createHeaders(),
			body: JSON.stringify({
				username,
				translations: [],
			}),
		})
		if (!response.ok) {
			throw new Error("Could not create user with username." + username)
		}
		const data = await response.json()
		return [null, data]
	} catch (error) {
		return [error.message, []]
	}
}

/**
 * Tries to find user if it exists login, if not then create a new user with the username provided.
 * @param {String} username
 * @returns {Object|Error} from createUser|checkForUser
 */
export const loginUser = async (username) => {
	const [checkError, user] = await checkForUser(username)

	if (checkError !== null) {
		return [checkError, null]
	}

	if (user.length > 0) {
		return [null, user.pop()]
	}

	return await createUser(username)
}

/**
 * Gets a users info from the database based on the id provided.
 * @param {Number} userId
 * @returns {Object|Error}
 */
export const userById = async (userId) => {
	try {
		const response = await fetch(`${apiUrl}/${userId}`)
		if (!response.ok) {
			throw new Error("Could not fetch user")
		}

		const user = await response.json()
		return [null, user]
	} catch (error) {
		return [error.message, null]
	}
}
