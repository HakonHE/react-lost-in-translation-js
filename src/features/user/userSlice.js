import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	username: "",
	userId: 0,
}

export const userSlice = createSlice({
	name: "user",
	initialState,
	reducers: {
		initUsername: (state, action) => {
			state.username = action.payload
		},
		initUserId: (state, action) => {
			state.userId = action.payload
		},
		deleteUsername: (state) => {
			state.username = ""
		},
		deleteUserId: (state) => {
			state.userId = 0
		},
	},
})

// Action creators are generated for each case reducer function
export const { initUsername, initUserId, deleteUsername, deleteUserId } = userSlice.actions

export default userSlice.reducer
