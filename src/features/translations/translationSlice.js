import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	translations: [],
}

export const translationSlice = createSlice({
	name: "translation",
	initialState,
	reducers: {
		initTranslation: (state, action) => {
			state.translations = action.payload
		},
		updateTranslation(state, action) {
			if (!state.translations || state.translations.length === 0) {
				state.translations = [state.translations, action.payload]
			} else {
				return {
					translations: [...state.translations, action.payload],
				}
			}
		},
		deleteTranslation: (state) => {
			state.translations = []
		},
	},
})

// Action creators are generated for each case reducer function
export const { initTranslation, updateTranslation, deleteTranslation } = translationSlice.actions

export default translationSlice.reducer
