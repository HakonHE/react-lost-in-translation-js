import { useSelector } from "react-redux"
import { Navigate } from "react-router-dom"

const withAuth = (Component) => (props) => {
	const username = useSelector((state) => state.user.username)
	if (username) {
		return <Component {...props} />
	} else {
		return <Navigate to="/" />
	}
}
export default withAuth
