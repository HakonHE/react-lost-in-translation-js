const ProfileList = ({ translation }) => {
	return <li className="border-b mb-1 border-slate-300">{translation}</li>
}

export default ProfileList
