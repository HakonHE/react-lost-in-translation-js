import React, { useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"
import { initUserId, initUsername } from "../../features/user/userSlice"
import { useForm } from "react-hook-form"
import { loginUser } from "../../api/user"
import { initTranslation } from "../../features/translations/translationSlice"
import { useNavigate } from "react-router-dom"

const usernameConfig = {
	required: true,
	minLength: 3,
}

const LoginForm = () => {
	const username = useSelector((state) => state.user.username)
	const navigate = useNavigate()
	const dispatch = useDispatch()

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm()

	useEffect(() => {
		if (username) {
			navigate("/translation")
		}
	}, [username, navigate])

	const onSubmit = async ({ username }) => {
		const usernameTrimmed = username.trim()
		if (usernameTrimmed.length > 0) {
			const [error, userResponse] = await loginUser(usernameTrimmed)

			if (userResponse !== null) {
				dispatch(initUsername(userResponse.username))
				dispatch(initUserId(userResponse.id))
				if (userResponse.translations) {
					dispatch(initTranslation(...userResponse.translations))
				}
			}
		}
	}

	const errorMessage = (() => {
		if (!errors.username) {
			return null
		}

		if (errors.username.type === "required") {
			return <span>Username is required</span>
		}

		if (errors.username.type === "minLength") {
			return <span>Username is too short(min. 3)</span>
		}
	})()

	return (
		<>
			<form
				onSubmit={handleSubmit(onSubmit)}
				className="container relative flex flex-row flex-nowrap justify-center content-evenly mt-8"
			>
				<fieldset className="">
					<input
						type="text"
						placeholder="What's your name?"
						{...register("username", usernameConfig)}
						className="rounded-full flex-auto basis-1/2 p-3 px-8 mr-2 border-2 border-slate-300"
					/>
					{errorMessage}
				</fieldset>
				<button
					type="submit"
					className="bg-NIGHT_PURPLE_LIGHT text-white rounded-full flex-initial border-2 px-2 py-1 hover:border-sky-500"
				>
					Login
				</button>
			</form>
		</>
	)
}

export default LoginForm
