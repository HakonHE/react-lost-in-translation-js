import { useSelector } from "react-redux"
import { NavLink } from "react-router-dom"

const Navbar = () => {
	const username = useSelector((state) => state.user.username)
	const translations = useSelector((state) => state.translation.translations)

	return (
		<>
			<div className="container h-20 grid grid-cols-2 grid-rows-1 p-4 bg-gradient-to-b from-DARK_NIGHT_BLUE to-NIGHT_BLUE rounded-t-md">
				<section className="text-white relative">
					{username && (
						<img
							src="images/Logo.png"
							alt="robot waving"
							width="55"
							className="float-left"
						/>
					)}
					<NavLink
						to="/translation"
						className="absolute top-2 left-20 text-xl font-bold font-sans"
					>
						Lost in Translation
					</NavLink>
				</section>
				<section className="relative text-white">
					{username && translations && (
						<p>
							<NavLink
								to="/profile"
								className="absolute bottom-1 right-4 rounded-full bg-transparent px-3 py-1 border-2 border-white hover:border-sky-500 hover:text-sky-500 tracking-wide"
							>
								Profile
							</NavLink>
						</p>
					)}
				</section>
			</div>
		</>
	)
}
export default Navbar
