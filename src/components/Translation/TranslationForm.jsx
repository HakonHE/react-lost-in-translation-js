import { useForm } from "react-hook-form"

const translationConfig = {
	maxLength: 40,
}

const TranslationForm = ({ onText }) => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm()

	const onSubmit = ({ textToTranslate }) => {
		const textToTranslateTrimmed = textToTranslate.trim()
		onText(textToTranslateTrimmed)
	}

	const errorMessage = (() => {
		if (!errors.textToTranslate) {
			return null
		}

		if (errors.textToTranslate.type === "maxLength") {
			return <span>Translation is too long(max. 40)</span>
		}
	})()

	return (
		<>
			<form
				onSubmit={handleSubmit(onSubmit)}
				className="relative flex flex-row justify-center content-evenly mt-12"
			>
				<fieldset>
					<input
						type="text"
						placeholder="What do you need translated?"
						{...register("textToTranslate", translationConfig)}
						className="rounded-full p-3 px-8 mr-2 flex-auto basis-1/2 w-96 border-2 border-slate-400 text-black focus:border-2 focus:border-black"
					/>
					{errorMessage}
				</fieldset>

				<button
					type="submit"
					className="bg-transparent text-white rounded-full border-2 px-2 py-1 flex-initial border-white hover:border-sky-500 hover:text-sky-500 tracking-wide"
				>
					Translate
				</button>
			</form>
		</>
	)
}

export default TranslationForm
