const TranslationOutputItem = ({ output }) => {
	return (
		<li key={output}>
			<img src={output} alt="sign language" width="55" />
		</li>
	)
}

export default TranslationOutputItem
